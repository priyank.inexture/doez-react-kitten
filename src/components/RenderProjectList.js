import React from 'react';
import PropTypes from 'prop-types';
// import { StyleSheet,FlatList } from 'react-native';
import { List,Divider,ListItem,Icon } from '@ui-kitten/components';
function RenderProjectList({ projects,route,navigation }) {
    const renderRightArrow = (props) => (
        <Icon {...props} name='arrow-forward-outline'/>
    );

    const renderItem = ({ item, index }) => (
        <ListItem
            onPress={() => navigation.navigate('Project Details', { id: item._id })}
            title={item.name}
            description={`${item.teamLead.firstName} ${item.teamLead.lastName}`}
            accessoryRight={renderRightArrow}
        />
    );
    return (
        <List
            data={projects}
            ItemSeparatorComponent={Divider}
            keyExtractor={item => item._id}
            renderItem={renderItem}
        />
    );
}
RenderProjectList.propTypes = {
    route: PropTypes.any,
    navigation: PropTypes.any,
    projects: PropTypes.array,
    isLoading: PropTypes.bool

};
export default RenderProjectList;
