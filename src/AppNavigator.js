import React,{ useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Drawer, DrawerItem, IndexPath } from '@ui-kitten/components';
import ProjectScreen from './screens/project';
import ResourceScreen from './screens/resource';
import { StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import LoginScreen from './screens/login';
import { getUserToken, storeUserToken } from './utils/asyncStorage';
import { getApplicationData } from './actions/utilAction';
import {
    signInAction,
    loadingAction,
    updateClientListAction,
    updateProjectListAction,
    updateResourceListAction,
    updateTypesListAction
} from './redux';
import ClientScreen from './screens/client';
import ResourceDetailScreen from './screens/resource/ResourceDetail';
import ClientDetailScreen from './screens/client/ClientDetail';
import ProjectDetailScreen from './screens/project/ProjectDetail';
const { Navigator, Screen } = createDrawerNavigator();
const Stack = createStackNavigator();

const mapStateToProps = (state) => {
    return {
        isLoading: state.app.isLoading,
        userToken: state.app.userToken,
        isSignout: state.app.isSignout,
        headerTitle: state.app.headerTitle
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        loadingActionProp: (isLoading) => dispatch(loadingAction(isLoading)),
        signInActionProp: (token) => dispatch(signInAction(token)),
        updateClientListActionProp: (clients) => dispatch(updateClientListAction(clients)),
        updateProjectListActionProp: (projects) => dispatch(updateProjectListAction(projects)),
        updateResourceListActionProp: (resources) => dispatch(updateResourceListAction(resources)),
        updateTypesListActionProp: (availableType) => dispatch(updateTypesListAction(availableType))
    };
};
function DrawerContent({ navigation, state }) {
    return (
        <Drawer
            selectedIndex={new IndexPath(state.index)}
            onSelect={index => navigation.navigate(state.routeNames[index.row])} style={{ marginTop: StatusBar.currentHeight }}>

            <DrawerItem title='Projects' />
            <DrawerItem title='Resources' />
            <DrawerItem title='Clients' />
        </Drawer>
    );
}
DrawerContent.propTypes = {
    navigation: PropTypes.any,
    state: PropTypes.any
};
const ProjectStackNavigator = () => {
    return (
        <Stack.Navigator initialRouteName='Project'>
            <Stack.Screen name='Project' component={ProjectScreen}/>
            <Stack.Screen name='Resource' component={ResourceScreen}/>
            <Stack.Screen name='Client' component={ClientScreen}/>
            <Stack.Screen name='Resource Details' component={ResourceDetailScreen}/>
            <Stack.Screen name='Client Details' component={ClientDetailScreen}/>
            <Stack.Screen name='Project Details' component={ProjectDetailScreen}/>
        </Stack.Navigator>
    );
};
const ResourceStackNavigator = () => {
    return (
        <Stack.Navigator initialRouteName='Resource'>
            <Stack.Screen name='Resource' component={ResourceScreen}/>
            <Stack.Screen name='Resource Details' component={ResourceDetailScreen}/>
        </Stack.Navigator>
    );
};
const ClientStackNavigator = () => {
    return (
        <Stack.Navigator initialRouteName='Client'>
            <Stack.Screen name='Client' component={ClientScreen}/>
            <Stack.Screen name='Client Details' component={ClientDetailScreen}/>
        </Stack.Navigator>
    );
};
export const DrawerNavigator = () => (
    <Navigator
    // drawerContent={props => <DrawerContent {...props}/>}
    >
        <Screen name='Projects' component={ProjectStackNavigator}/>
        <Screen name='Resources' component={ResourceStackNavigator}/>
        <Screen name='Clients' component={ClientStackNavigator}/>
    </Navigator>
);
export const DrawerLoginNavigator = () => (
    <Navigator>
        <Screen name='Login' component={LoginScreen}/>
    </Navigator>
);
function AppNavigator({
    updateClientListActionProp,
    updateProjectListActionProp,
    updateResourceListActionProp,
    updateTypesListActionProp,
    signInActionProp,
    loadingActionProp,
    isLoading,
    userToken,
    isSignout,
    headerTitle
}) {
    useEffect(() => {
        getUserToken().then(token => {
            console.log('found token: ', token);
            // if (token) {
            //     signInActionProp(token);
            //     storeUserToken(token);
            //     // storeUserToken(null);
            // } else {
            //     storeUserToken(null);
            // }
            storeUserToken(null);
        }).finally(() => loadingActionProp(false));
    }, []);
    useEffect(() => {
        if (userToken) {
            getApplicationData().then(({ data }) => {
                console.log('recived',data.projects.length);
                // console.log(data);
                updateClientListActionProp(data.clients);
                updateProjectListActionProp(data.projects);
                updateResourceListActionProp(data.employees);
                // updateTypesListActionProp(data.availableType);
            });
        }
    }, [userToken]);
    return (
        <NavigationContainer>
            {
                userToken && <DrawerNavigator/>
            }
            {
                !userToken && <DrawerLoginNavigator/>
            }
        </NavigationContainer>
    );
}
AppNavigator.propTypes = {
    signInActionProp: PropTypes.func,
    loadingActionProp: PropTypes.func,
    updateClientListActionProp: PropTypes.func,
    updateProjectListActionProp: PropTypes.func,
    updateResourceListActionProp: PropTypes.func,
    updateTypesListActionProp: PropTypes.func,
    isLoading: PropTypes.bool,
    userToken: PropTypes.any,
    isSignout: PropTypes.bool,
    headerTitle: PropTypes.string
};
export default connect(mapStateToProps, mapDispatchToProps)(AppNavigator);
