/* eslint-disable max-nested-callbacks */
import React,{ useEffect,useState } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet,FlatList } from 'react-native';
import { connect } from 'react-redux';
import { Layout, Spinner,Text } from '@ui-kitten/components';
import { loadingAction } from '../../redux';
import { getResourceInfo } from '../../actions/ResourceAction';
import Field from '../../components/Field';
import RenderProjectList from '../../components/RenderProjectList';
const mapStateToProps = (state) => {
    return {
        isLoading: state.app.isLoading,
        resources: state.resource.list,
        projects: state.project.list
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        loadingActionProp: (isLoading) => dispatch(loadingAction(isLoading))
    };
};

function ResourceDetailScreen({
    projects,
    resources,
    route,
    navigation,
    isLoading,
    loadingActionProp
}) {
    const { id } = route.params;
    const [resource, setResource] = useState({
        designation: '',
        technology: [],
        firstName: '',
        lastName: '',
        userName: '',
        email: '',
        contactNumber: '',
        address: '',
        projects: []
    });
    useEffect(() => {
        loadingActionProp(true);
        getResourceInfo(id)
            .then(({ data }) => {
                console.log(data);
                let tech = data.resource.technology.map(t => t.value);
                setResource({
                    ...data.resource,
                    technology: tech ? tech : [],
                    projects: data.responsibilities.map(r => {
                        let project = projects.find(p => p._id === r.project);
                        if (project) {
                            project.responsibility = r;
                            project.role = r.role;
                            return project;
                        }
                        return null;
                    }).filter(r => r)
                });
            })
            .catch(err => console.log(err))
            .finally(() => loadingActionProp(false));
    }, [resources, projects]);

    return (
        <Layout style={styles.mainContainer}>
            {isLoading && <Layout style={styles.centerContent}><Spinner/></Layout>}
            {!isLoading && <>
                <Layout>
                    <FlatList

                        data={[
                            { title: 'First Name', text: resource.firstName },
                            { title: 'Last Name', text: resource.lastName },
                            { title: 'Email', text: resource.email },
                            { title: 'Contact Number', text: resource.contactNumber },
                            { title: 'Technologies', text: resource.technology.join(', ') }
                        ]}
                        keyExtractor={(_,index) => index.toString()}
                        numColumns={1}
                        renderItem={({ item }) => (
                            <Field title={item.title} text={item.text}/>
                        )}
                    />
                    <Text style={styles.text} category='h5'>Projects</Text>
                    <RenderProjectList
                        projects={resource.projects}
                        route={route}
                        navigation={navigation}/>
                </Layout>
            </>
            }
        </Layout>
    );
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    centerContent: {
        flex: 1,
        // flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: '50%',
        marginBottom: 15
    },
    text: {
        marginVertical: 10,
        marginHorizontal: 15
    }

});
ResourceDetailScreen.propTypes = {
    route: PropTypes.any,
    navigation: PropTypes.any,
    isLoading: PropTypes.bool,
    projects: PropTypes.array,
    resources: PropTypes.array,
    loadingActionProp: PropTypes.func

};
export default connect(mapStateToProps, mapDispatchToProps)(ResourceDetailScreen);
