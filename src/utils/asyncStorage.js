import AsyncStorage from '@react-native-async-storage/async-storage';
import { setUserToken } from './APIKit';
export const storeUserToken = async(value) => {
    try {
        const jsonValue = JSON.stringify(value);
        setUserToken(value);
        await AsyncStorage.setItem('userToken', jsonValue);
    } catch (e) {
        // saving error
    }
};
export const getUserToken = async() => {
    try {
        const jsonValue = await AsyncStorage.getItem('userToken');
        return jsonValue !== null ? JSON.parse(jsonValue) : null;
    } catch (e) {
        // error reading value
    }
    return null;
};
