import {
    SIGN_IN_ACTION,
    SIGN_OUT_ACTION,
    RESTORE_TOKEN_ACTION,
    LOADING_ACTION,
    CHANGE_HEADER_ACTION
} from './appType';
const initialState = {
    isLoading: true,
    isSignout: true,
    userToken: null,
    headerTitle: 'Doez'
};
const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case SIGN_IN_ACTION:
            return {
                ...state,
                isSignout: action.isSignout,
                userToken: action.userToken
            };
        case SIGN_OUT_ACTION:
            return {
                ...state,
                isSignout: action.isSignout,
                userToken: action.userToken
            };
        case RESTORE_TOKEN_ACTION:
            return {
                ...state,
                userToken: action.userToken,
                isLoading: action.isLoading
            };
        case LOADING_ACTION:
            return {
                ...state,
                isLoading: action.isLoading
            };
        case CHANGE_HEADER_ACTION:
            return {
                ...state,
                headerTitle: action.headerTitle
            };
        default:
            // console.log(new Error(`From app reducer Action: ${action.type} Not found`));
            return state;
    }
};
export default appReducer;
