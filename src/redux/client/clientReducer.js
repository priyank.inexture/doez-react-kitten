import { UPDATE_CLIENTS_ACTION } from './clientType';
const initialState = {
    list: [],
    updated: new Date()
};
const clientReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_CLIENTS_ACTION:
            return {
                ...state,
                list: action.clients,
                updated: new Date()
            };
        default:
            // console.log(new Error(`Action: ${action.type} Not found`));
            return state;
    }
};
export default clientReducer;
