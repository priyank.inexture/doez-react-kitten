import { APIKit } from '../utils/APIKit';
import { RESOURCE_URL } from '../constants/api';
export const getResourceInfo = (id) => APIKit.get(`${RESOURCE_URL}/${id}`);
