import React from 'react';
import { SafeAreaView } from 'react-native';
import ClientList from './ClientList';

function ClientScreen(props) {
    return (
        <SafeAreaView>
            <ClientList {...props}/>
        </SafeAreaView>
    );
}

export default ClientScreen;
