import axios from 'axios';
import { API_HOST } from '../constants/api';
import AsyncStorage from '@react-native-async-storage/async-storage';
// import { storeUserToken } from './asyncStorage';
import store from '../redux/store';
import { signOutAction } from '../redux';
// Create axios client, pre-configured with baseURL
export const APIKit = axios.create({
    baseURL: API_HOST,
    timeout: 10000
});
export const APIKitWithoutAuthorization = axios.create({
    baseURL: API_HOST,
    timeout: 10000
});
const storeUserToken = async(value) => {
    try {
        const jsonValue = JSON.stringify(value);
        setUserToken(value);
        await AsyncStorage.setItem('userToken', jsonValue);
    } catch (e) {
        // saving error
    }
};
const responseInterceptor = (response) => {
    // console.log('I am here');
    if (response.status === 401 || response.status === 403) {
        store.dispatch(signOutAction());
        storeUserToken(null);
    }
    if (response.config.parse) {
        //perform the manipulation here and change the response object
    }
    return response;
};
const errorHandling = (error) => {
    if (error.response.status === 401 || error.response.status === 403) {
        store.dispatch(signOutAction());
        storeUserToken(null);
    }
    if (error.response && error.response.data) {
        return Promise.reject(error.response.data.error ? error.response.data.error : error.response.data);
    }
    store.dispatch(signOutAction());
    return Promise.reject(error.message);
};
APIKit.interceptors.response.use(responseInterceptor,errorHandling);
APIKitWithoutAuthorization.interceptors.response.use(responseInterceptor,errorHandling);
// Set JSON Web Token in Client to be included in all calls
export const setUserToken = token => {
    console.log('setting token',token);
    APIKit.interceptors.request.use(function requestInterceptor(config) {
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    });
};
