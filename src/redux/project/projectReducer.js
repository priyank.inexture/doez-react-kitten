import { UPDATE_PROJECTS_ACTION } from './projectType';
const initialState = {
    list: [],
    updated: new Date()
};
const projectReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_PROJECTS_ACTION:
            return {
                ...state,
                list: action.projects,
                updated: new Date()
            };
        default:
            // console.log(new Error(`Action: ${action.type} Not found`));
            return state;
    }
};
export default projectReducer;
