import { APIKit } from '../utils/APIKit';
import { PROJECT_URL } from '../constants/api';
export const getProjectInfo = (id) => APIKit.get(`${PROJECT_URL}/${id}`);
