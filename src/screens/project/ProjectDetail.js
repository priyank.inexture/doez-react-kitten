import React,{ useEffect,useState } from 'react';
import PropTypes from 'prop-types';
import { View,FlatList } from 'react-native';
import { connect } from 'react-redux';
import { StyleSheet } from 'react-native';
import { Button,Layout,Spinner } from '@ui-kitten/components';
import { getProjectInfo } from '../../actions/projectAction';
import { loadingAction } from '../../redux';
import Field from '../../components/Field';
const mapStateToProps = (state) => {
    return {
        isLoading: state.app.isLoading
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        loadingActionProp: (isLoading) => dispatch(loadingAction(isLoading))
    };
};
function ProjectDetailScreen({ route,
    navigation,
    isLoading,
    loadingActionProp
}) {
    const { id } = route.params;
    const [project, setProject] = useState({ allocation: '',
        amount: 0,
        clientList: [],
        name: '',
        paymentType: '',
        priority: '',
        reviewer: {
            address: '',
            contactNumber: '',
            email: '',
            firstName: '',
            lastName: '',
            userName: ''
        },
        status: '',
        teamLead: {
            address: '',
            contactNumber: '',
            email: '',
            firstName: '',
            lastName: '',
            userName: ''
        },
        _id: '',
        tech: [],
        responsibilities: [] });
    useEffect(() => {
        loadingActionProp(true);
        getProjectInfo(id)
            .then(({ data }) => {
                console.log(data);
                setProject({
                    ...data.project,
                    // eslint-disable-next-line max-nested-callbacks
                    tech: data.project.tech.map(t => t.value),
                    responsibilities: data.responsibilities
                });
            })
            .catch(err => console.log(err))
            .finally(() => loadingActionProp(false));
    }, []);
    return (
        <Layout style={styles.mainContainer}>
            {isLoading && <Layout style={styles.centerContent}><Spinner/></Layout>}
            {!isLoading && <>

                <Layout>
                    <FlatList

                        data={[
                            { title: 'Name', text: project.name },
                            { title: 'Payment Type', text: project.paymentType },
                            { title: 'Amount', text: project.amount },
                            { title: 'Allocation', text: project.allocation },
                            { title: 'Status', text: project.status },
                            { title: 'Priority', text: project.priority },
                            { title: 'Technologies', text: project.tech.join(', ') },
                            { title: 'Team Lead', text: project.teamLead ? `${project.teamLead.firstName} ${project.teamLead.lastName}` : null },
                            { title: 'Reviewer', text: project.reviewer ? `${project.reviewer.firstName} ${project.reviewer.lastName}` : null }
                        ]}
                        keyExtractor={(_,index) => index.toString()}
                        numColumns={1}
                        renderItem={({ item }) => (
                            <Field title={item.title} text={item.text}/>
                        )}
                    />
                </Layout>
                <Layout level='1'>
                    <View style={styles.container}>

                        <Button
                            style={styles.button}
                            appearance='outline'
                            onPress={() => navigation.navigate('Client',{ clients: project.clientList })}>
                            {project.clientList.length} clients
                        </Button>

                        <Button
                            style={styles.button}
                            appearance='filled'
                            onPress={() => navigation.navigate('Resource',{ responsibilities: project.responsibilities })}>
                            {project.responsibilities.length} resources
                        </Button>
                    </View>
                </Layout>
                {/* <Layout style={styles.layout} level='4'>
                    <Text>12312314</Text>
                </Layout>

                <Layout style={styles.layout} level='3'>
                    <Text>12313123</Text>
                </Layout> */}
            </>
            }
        </Layout>

    );
}
const styles = StyleSheet.create({
    centerContent: {
        flex: 1,
        // flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between'
        // alignItems: 'center'
    },
    // layout: {
    //     flex: 1,
    //     justifyContent: 'center',
    //     alignItems: 'center'
    // },
    container: {
        // flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        marginHorizontal: 15,
        marginTop: 25
    },
    button: {
        width: '48%',
        marginBottom: 15
    }
});
ProjectDetailScreen.propTypes = {
    route: PropTypes.any,
    navigation: PropTypes.any,
    isLoading: PropTypes.bool,
    loadingActionProp: PropTypes.func

};
export default connect(mapStateToProps, mapDispatchToProps)(ProjectDetailScreen);
