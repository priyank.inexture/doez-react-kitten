/* eslint-disable max-nested-callbacks */
import React,{ useEffect,useState } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet,FlatList } from 'react-native';
import { connect } from 'react-redux';
import { Layout, Spinner,Text } from '@ui-kitten/components';
import { loadingAction } from '../../redux';
import { getClientInfo } from '../../actions/ClientAction';
import Field from '../../components/Field';
import RenderProjectList from '../../components/RenderProjectList';
const mapStateToProps = (state) => {
    return {
        isLoading: state.app.isLoading,
        clients: state.client.list,
        projects: state.project.list
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        loadingActionProp: (isLoading) => dispatch(loadingAction(isLoading))
    };
};

function ClientDetailScreen({
    projects,
    clients,
    route,
    navigation,
    isLoading,
    loadingActionProp
}) {
    const { id } = route.params;
    const [client, setClient] = useState({
        firstName: '',
        lastName: '',
        email: '',
        contactNumber: '',
        address: '',
        projects: []
    });
    useEffect(() => {
        loadingActionProp(true);
        getClientInfo(id)
            .then(({ data }) => {
                console.log(data);
                setClient({
                    ...data.client,
                    projects: data.projects.map(r => projects.find(p => p._id === r._id)).filter(p => p)
                });
            })
            .catch(err => console.log(err))
            .finally(() => loadingActionProp(false));
    }, [clients, projects]);

    return (
        <Layout style={styles.mainContainer}>
            {isLoading && <Layout style={styles.centerContent}><Spinner/></Layout>}
            {!isLoading && <>
                <Layout>
                    <FlatList

                        data={[
                            { title: 'First Name', text: client.firstName },
                            { title: 'Last Name', text: client.lastName },
                            { title: 'Email', text: client.email },
                            { title: 'Contact Number', text: client.contactNumber }
                        ]}
                        keyExtractor={(_,index) => index.toString()}
                        numColumns={1}
                        renderItem={({ item }) => (
                            <Field title={item.title} text={item.text}/>
                        )}
                    />
                    <Text style={styles.text} category='h5'>Projects</Text>
                    <RenderProjectList
                        projects={client.projects}
                        route={route}
                        navigation={navigation}/>
                </Layout>
            </>
            }
        </Layout>
    );
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    centerContent: {
        flex: 1,
        // flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: '50%',
        marginBottom: 15
    },
    text: {
        marginVertical: 10,
        marginHorizontal: 15
    }

});
ClientDetailScreen.propTypes = {
    route: PropTypes.any,
    navigation: PropTypes.any,
    isLoading: PropTypes.bool,
    projects: PropTypes.array,
    clients: PropTypes.array,
    loadingActionProp: PropTypes.func

};
export default connect(mapStateToProps, mapDispatchToProps)(ClientDetailScreen);

