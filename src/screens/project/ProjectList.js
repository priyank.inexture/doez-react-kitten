import React from 'react';
// import { StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import { Divider, List, ListItem,Icon } from '@ui-kitten/components';
import RenderProjectList from '../../components/RenderProjectList';
const mapStateToProps = (state) => {
    return {
        projects: state.project.list
    };
};
const mapDispatchToProps = (dispatch) => {
    return {

    };
};
function ProjectList({ route,navigation,projects }) {
    return (
        <RenderProjectList
            projects={projects}
            route={route}
            navigation={navigation}/>
    );
}
ProjectList.propTypes = {
    projects: PropTypes.array,
    navigation: PropTypes.any,
    route: PropTypes.any
};
export default connect(mapStateToProps, mapDispatchToProps)(ProjectList);
