import React from 'react';
import { SafeAreaView } from 'react-native';
import ResourceList from './ResourceList';
// const BackIcon = (props) => (
//     <Icon {...props} name='arrow-back'/>
// );

// const BackAction = () => (
//     <TopNavigationAction icon={BackIcon}/>
// );

function ResourceScreen(props) {
    return (
        <SafeAreaView
        // style={{ marginTop: StatusBar.currentHeight }}
        >
            {/* <TopNavigation
                title={evaProps => <Text {...evaProps}>Resource List</Text>}
                // subtitle={evaProps => <Text {...evaProps}>Subtitle</Text>}
            /> */}
            <ResourceList {...props}/>
        </SafeAreaView>
    );
}

export default ResourceScreen;
