import React from 'react';
import { StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Divider, List, ListItem,Icon } from '@ui-kitten/components';

const mapStateToProps = (state) => {
    return {
        clients: state.client.list
    };
};
const mapDispatchToProps = (dispatch) => {
    return {

    };
};
function ClientList({ route,navigation,clients }) {
    console.log('route',route);
    const renderRightArrow = (props) => (
        <Icon {...props} name='arrow-forward-outline'/>
    );
    const renderItem = ({ item, index }) => (
        <ListItem
            title={`${item.firstName} ${item.lastName}`}
            onPress={() => navigation.navigate('Client Details', { id: item._id })}
            // description={`${item.teamLead.firstName} ${item.teamLead.lastName}`}
            accessoryRight={renderRightArrow}
        />
    );
    if (route.params && route.params.clients) {
        return (
            <List
                style={styles.container}
                data={route.params.clients}
                keyExtractor={item => item._id}
                ItemSeparatorComponent={Divider}
                renderItem={renderItem}
            />
        );
    }
    return (
        <List
            style={styles.container}
            data={clients}
            keyExtractor={item => item._id}
            ItemSeparatorComponent={Divider}
            renderItem={renderItem}
        />
    );
}
ClientList.propTypes = {
    clients: PropTypes.array,
    navigation: PropTypes.any,
    route: PropTypes.any
};
const styles = StyleSheet.create({
    container: {
        // flex: 1
    }
});
export default connect(mapStateToProps, mapDispatchToProps)(ClientList);
