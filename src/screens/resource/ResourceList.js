import React from 'react';
import { StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Divider, List, ListItem,Icon } from '@ui-kitten/components';

const mapStateToProps = (state,ownParams) => {
    const { route } = ownParams ;
    console.log('here',route);
    if (route.params.responsibilities) {
        const responsibilities = route.params.responsibilities ? route.params.responsibilities.map(r => ({ ...r.employee,role: r.role })) : [];
        console.log('new',responsibilities);
        return {
            resources: responsibilities
        };
    }
    return {
        resources: state.resource.list
    };
};
const mapDispatchToProps = (dispatch) => {
    return {

    };
};
function ResourceList({ navigation,resources }) {
    const renderRightArrow = (props) => (
        <Icon {...props} name='arrow-forward-outline'/>
    );
    const renderItem = ({ item, index }) => (
        <ListItem
            onPress={() => navigation.navigate('Resource Details',{ id: item._id })}
            title={`${item.firstName} ${item.lastName}`}
            description={item.role ? item.role : ''}
            accessoryRight={renderRightArrow}
        />
    );

    return (
        <List
            style={styles.container}
            data={resources}
            ItemSeparatorComponent={Divider}
            keyExtractor={item => item._id}
            renderItem={renderItem}
        />
    );
}
ResourceList.propTypes = {
    resources: PropTypes.array,
    navigation: PropTypes.any
};
const styles = StyleSheet.create({
    container: {
        // flex: 1
    }
});
export default connect(mapStateToProps, mapDispatchToProps)(ResourceList);
