import React from 'react';
import { registerRootComponent } from 'expo';
import { IconRegistry } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { Provider } from 'react-redux';
import store from './src/redux/store';
import App from './src/App';
const AppContainer = () => (
    <>
        <IconRegistry icons={EvaIconsPack}/>
        <Provider store={store}>
            <App/>
        </Provider>
    </>
);
export default registerRootComponent(AppContainer);
