import React, { useState } from 'react';
import { SafeAreaView,View, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Input, CheckBox, StyleService, useStyleSheet, Icon,Text } from '@ui-kitten/components';
import { postLoginCredentials } from '../../actions/utilAction';
import { storeUserToken } from '../../utils/asyncStorage';
import { signInAction } from '../../redux';
const mapStateToProps = (state) => {
    return {

    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        signInActionProp: (token) => dispatch(signInAction(token))
    };
};

function LoginScreen({ signInActionProp }) {
    const [username, setUsername] = useState('admin');
    const [password, setPassword] = useState('admin');
    const [isSuperUser, setIsSuperUser] = useState(true);
    const [passwordVisible, setPasswordVisible] = useState(false);

    const styles = useStyleSheet(themedStyles);

    const onPasswordIconPress = () => {
        setPasswordVisible(!passwordVisible);
    };

    const renderPasswordIcon = (props) => (
        <TouchableWithoutFeedback onPress={onPasswordIconPress}>
            <Icon {...props} name={passwordVisible ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    );
    const renderUserIcon = (props) => (
        <Icon name="person" {...props}/>
    );
    const handleButtonPress = () => {
        postLoginCredentials({
            user: {
                isSuperUser: isSuperUser,
                username: username,
                password: password
            }
        }).then(({ data }) => {
            if (data) {
                const { token } = data;
                signInActionProp(token);
                storeUserToken(token);
            } else {
                throw new Error('Some error occ');
            }
        }).catch((error) => {
            if (error.response.data.error) {
                // Toast.show({
                //     text: error.response.data.error,
                //     type: 'danger'
                // });
                console.log(error.response.data.error);
            } else {
                // Toast.show({
                //     text: 'Something went wrong!',
                //     type: 'danger'
                // });
                console.log('Something went wrong!');
            }
        });
    };
    return (
        // <SafeAreaView style={{ marginTop: StatusBar.currentHeight }}>
        <SafeAreaView>
            {/* <TopNavigation
                title={evaProps => <Text {...evaProps}>Login Screen</Text>}
                subtitle={evaProps => <Text {...evaProps}>Subtitle</Text>}
            /> */}
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <Text category='h1' status='control'> Hello </Text>
                    <Text style={styles.signInLabel} category='s1' status='control'> Sign in to your account</Text>
                </View>
                <View style={styles.formContainer}>
                    <Input
                        placeholder='Username'
                        accessoryRight={renderUserIcon}
                        value={username}
                        onChangeText={nextValue => setUsername(nextValue)}
                    />
                    <Input
                        placeholder='Password'
                        accessoryRight={renderPasswordIcon}
                        value={password}
                        secureTextEntry={!passwordVisible}
                        style={styles.passwordInput}
                        onChangeText={setPassword}
                    />
                    <CheckBox
                        checked={isSuperUser}
                        onChange={nextChecked => setIsSuperUser(nextChecked)}>
                        Is Super User
                    </CheckBox>
                </View>
            </View>
            <Button
                style={styles.signInButton}
                size='giant' onPress={handleButtonPress}>
        SIGN IN
            </Button>
        </SafeAreaView>
    );
}
const themedStyles = StyleService.create({
    container: {
        backgroundColor: 'background-basic-color-1'
    },
    headerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: 216,
        backgroundColor: 'color-primary-default'
    },
    formContainer: {
        // flex: 1,
        paddingVertical: 32,
        paddingHorizontal: 16
        // borderWidth: 1
    },
    signInLabel: {
        marginTop: 16
    },
    signInButton: {
        marginHorizontal: 16
    },
    signUpButton: {
        marginVertical: 12,
        marginHorizontal: 16
    },
    forgotPasswordContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    passwordInput: {
        marginVertical: 16
    },
    forgotPasswordButton: {
        paddingHorizontal: 0
    }
});
LoginScreen.propTypes = {
    signInActionProp: PropTypes.func
};
export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
// export default LoginScreen;
