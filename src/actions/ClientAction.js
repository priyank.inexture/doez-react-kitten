import { APIKit } from '../utils/APIKit';
import { CLIENT_URL } from '../constants/api';
export const getClientInfo = (id) => APIKit.get(`${CLIENT_URL}/${id}`);
