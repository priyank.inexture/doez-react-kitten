import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet,ScrollView } from 'react-native';
import { Text,Subtitle,ListItem, Right, List, Icon,Title, Left, Container, Tabs, Tab, Body, View } from 'native-base';
const mapStateToProps = (state,ownProps) => {
    // const responsibilities = ownProps.route.params.responsibilities;
    // return {
    //     responsibilities: responsibilities
    // };
    return {};
};
const mapDispatchToProps = (dispatch) => {
    return {

    };
};

const ProjectClientResourceList = ({ clients,responsibilities,navigation }) => {
    const handleDeleteClient = (id) => {
        console.log('deleting client', id);
    };
    const handleDeleteResource = (id) => {
        console.log('deleting resource', id);
    };
    const handleEditResource = (id) => {
        console.log('editing resource', id);
        navigation.navigate('Edit Resource', { id: id });
    };
    const handleDetailResource = (id) => {
        console.log('detail resource', id);
        navigation.navigate('Resource Detail', { id: id });
    };
    const handleDetailClient = (id) => {
        console.log('detail client', id);
        navigation.navigate('Client Detail', { id: id });
    };
    return (
        <Container style={{ height: 500 }}>
            <Tabs>
                <Tab heading="Clients">
                    <ScrollView>
                        {clients.length === 0 && <Text style={styles.empty}>No clients</Text>}
                        <List>
                            {clients.map(c =>
                                <ListItem key={c._id}
                                // onPress={() => navigation.navigate('Resource Detail', {
                                //     id: c._id
                                // })}
                                >
                                    <Left>
                                        <Body>
                                            <Title style={styles.text}>
                                                {c.firstName} {c.lastName}
                                            </Title>
                                            {/* <Subtitle style={styles.subtext}>
                                                {r.role}
                                            </Subtitle> */}
                                        </Body>
                                    </Left>
                                    <Right>
                                        <View style={styles.iconContainerTwo}>
                                            <Icon type="MaterialCommunityIcons" name="delete" style={styles.deleteIcon} onPress={() => handleDeleteClient(c._id)}/>
                                            <Icon name="arrow-forward" style={styles.detailIcon} onPress={() => handleDetailClient(c._id)}/>
                                        </View>
                                    </Right>
                                </ListItem>
                            )}
                        </List>
                    </ScrollView>
                </Tab>
                <Tab heading="Resources">
                    <ScrollView>
                        {responsibilities.length === 0 && <Text style={styles.empty}>No Resources</Text>}
                        <List>
                            {responsibilities.map(r =>
                                <ListItem key={r._id}>
                                    <Left>
                                        <Body>
                                            <Title style={styles.text}>
                                                {r.employee.firstName} {r.employee.lastName}
                                            </Title>
                                            <Subtitle style={styles.subtext}>
                                                {r.role}
                                            </Subtitle>
                                        </Body>
                                    </Left>
                                    <Right>
                                        <View style={styles.iconContainer}>
                                            <Icon type="MaterialIcons" name="edit" style={styles.editIcon} onPress={() => handleEditResource(r._id)}/>
                                            <Icon type="MaterialCommunityIcons" name="delete" style={styles.deleteIcon} onPress={() => handleDeleteResource(r._id)}/>
                                            <Icon name="arrow-forward" style={styles.detailIcon}
                                                onPress={() => handleDetailResource(r.employee._id)}
                                            />
                                        </View>
                                    </Right>
                                </ListItem>
                            )}
                        </List>
                    </ScrollView>
                </Tab>
            </Tabs>
        </Container>
    );
};
const styles = StyleSheet.create({
    text: {
        color: 'black'
    },
    subtext: {
        color: '#747474'
    },
    empty: {
        marginTop: 25,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    iconContainer: {
        flex: 1 ,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: 100,
        paddingHorizontal: 5
    },
    iconContainerTwo: {
        flex: 1 ,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: 50,
        paddingHorizontal: 5
    },
    detailIcon: {
        color: '#747474'
    },
    deleteIcon: {
        color: '#d9534f'
    },
    editIcon: {
        color: '#3F51B5'
    }
});
ProjectClientResourceList.propTypes = {
    clients: PropTypes.array,
    responsibilities: PropTypes.array,
    navigation: PropTypes.any
};
export default connect(mapStateToProps, mapDispatchToProps)(ProjectClientResourceList);
