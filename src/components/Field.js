import React from 'react';
import PropTypes from 'prop-types';
import { Text } from '@ui-kitten/components';
import { StyleSheet ,View } from 'react-native';
function Field({ title, text }) {
    return (
        <View style={styles.container}>
            <View style={styles.titleContainer}>
                <Text style={styles.title}>{title}</Text>
            </View>
            <View style={styles.textContainer}>
                <Text style={styles.text}>{text ? text : 'Not Assigned'}</Text>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 8,
        flexDirection: 'row'
    },
    titleContainer: {
        flex: 1,
        margin: 5
    },
    textContainer: {
        flex: 2,
        margin: 5
    },
    title: {
        // fontWeight: 'bold',
        textAlign: 'left',
        color: '#747474'
    },
    text: {
        textAlign: 'right'
    }
});
Field.propTypes = {
    title: PropTypes.any,
    text: PropTypes.any
};
export default Field;
