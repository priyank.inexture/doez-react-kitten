import React from 'react';
import { SafeAreaView } from 'react-native';
import ProjectList from './ProjectList';
// s

function ProjectScreen(props) {
    return (
        <SafeAreaView >
            <ProjectList {...props}/>
        </SafeAreaView>
    );
}

export default ProjectScreen;
