import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    Container,
    Header,
    Content,
    List,
    ListItem,
    Text,
    Button
} from 'native-base';
import { testAction } from '../redux';
const mapStateToProps = (state) => {
    return {
        testList: state.test.testList
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        testActionProp: (randomValue) => dispatch(testAction(randomValue))
    };
};
function ListExample({ testActionProp,testList }) {
    return (
        <Container>
            <Header />
            <Content>
                <Button onPress={() => testActionProp(Math.random())}>
                    <Text>Add Updated</Text>
                </Button>
                <List>
                    {testList.map((listItem, index) => (
                        <ListItem key={index}>
                            <Text>{listItem}</Text>
                        </ListItem>
                    ))}
                </List>
            </Content>
        </Container>
    );
}
ListExample.propTypes = {
    testActionProp: PropTypes.func,
    testList: PropTypes.array
};
export default connect(mapStateToProps, mapDispatchToProps)(ListExample);
